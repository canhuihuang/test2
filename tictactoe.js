const generateMatrix = (cellsPerLine, options = {fillWithCount: false}) => {
    const {fillWithCount} = options;

    const matrix = [];

    let count = 1;
    for (let j = 0; j < cellsPerLine; j++) {
        const row = []
        for (let i = 0; i < cellsPerLine; i++) {
            if (fillWithCount === true) row.push(count);
            else row.push(undefined);
            count++;
        }
        matrix.push(row);
    }
    return matrix
}

const getUserInput = () => {

    console.log('')
}

const matrix = generateMatrix(3,{fillWithCount: true})
let turn = 1;   // 1 for O, -1 for X
let winner = 0;

// Show instructions

// Start game
// while (winner === 0){

//     // Show board
//     console.log(matrix);

//     // Insert piece
//     getUserInput();
// }


console.log(matrix)

// const m = [[1,2,3],[4,5,6],[7,8,9]]

// for (let j = 0; j < m.length; j++) {
//     for (let i = 0; i < m.length; i++) {
//         console.log(m[j][i]);
//     }
// }
